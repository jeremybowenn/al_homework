;
; arraystarter.asm
;
; Set up a simple array of ints in memory and then play around with it.
;
; @author  Terry Sergeant
; @version Fall 2016
;
%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

		section .data
myarray:	dd	0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xAA
file_name:	db	"number.txt", 0
endl:		db	10,0
		
		section .bss
a:		resd	100

		section .text
		global 		main
main:
		; before running this guess what the output will be ... THEN run it.
;		put_i	[myarray]
;		put_str	endl

		fopenr	rbx, file_name
		xor	r9, r9
		xor	eax, eax
loop:		cmp	eax, -1 
		je	done
		fget_i	rbx, ecx
		mov	[a+4*r9], ecx
		inc	r9
		jmp	loop

done:		fclosem	rbx	

		xor	r10, r10
loop2:		cmp	r10, r9
		jge	alldone
		put_i	[a+4*r10]
		put_str	endl
		inc	r10
		jmp	loop2

		; write the necessary statements, without using a loop, to display all elements of myarray
;		put_i	[myarray+4]
;		put_str	endl
;		put_i	[myarray+8]
;		put_str	endl
;		put_i	[myarray+12]
;		put_str	endl
;		put_i	[myarray+16]
;		put_str	endl
;		put_i	[myarray+24]
;		put_str	endl
;		put_i	[myarray+28]
;		put_str	endl
;		put_i	[myarray+32]
;		put_str	endl
;		put_i	[myarray+36]
;		put_str	endl
;		put_i	[myarray+40]
;		put_str	endl




		; now display myarray using a loop
;		xor	rax, rax
;loop:		cmp	eax, 10
;		jge	done
;		put_i	[myarray+4*eax]
;		put_str	endl
;		inc	rax
;		jmp	loop
;done:



		; bring a printout of your completed source code to labday

alldone: 	mov	ebx,0		; return 0
		mov	eax,1		; on
		int	80h		; exit

