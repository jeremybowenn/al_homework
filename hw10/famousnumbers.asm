;
; Demonstrate I/O issues with floating point numbers.
;
; @author  Terry Sergeant
;

%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

		section .data
pi:		dq	3.141592653589793       ; PI
e:		dq	2.71828182845904523536  ; Euler's constant
phi:		dq	1.6180339887            ; golden ratio
av:		dq	6.022141e23             ; Avagadro's number
gr:		dq	6.674e-11               ; gravitational constant
one:		dq	1.0			; one
estimate	dq	3.14
cutoff:		dq	0.0001
answer:		db	"Answer is: " , 0
try:		db	"Try again", 0
endl:		db	10,0


		section .bss
ans:		resq	1
error:		resq	1

		section .text

		global 		main
		extern	fabs
main:
		movsd	xmm7,[pi]	; put pi in xmm0
		movsd	xmm1,[e]	; put e in xmm1
		movsd	xmm2,[phi]	; put phi in xmm2
		movsd	xmm3,[av]	; put av in xmm3
		movsd	xmm4,[gr]	; put gr in xmm4
		movsd	xmm5,[one]	; put one in xmm5

		movsd	xmm0,[pi]
;		addsd	xmm0,[e]
		movsd	[ans],xmm0
		
;		put_dbl	[ans]
;		put_str	endl

		; NOTE: put_dbl destroys some of the xmm registers
		; Add statements below to figure out which xmm registers got messed up
		movsd	xmm8, [estimate]
		movsd	xmm9, [cutoff]
		subsd	xmm7, xmm8	; error
		call	fabs
		ucomisd	xmm7, xmm9	; error <= cutoff
		ja	else
		put_str	answer
		put_dbl	[estimate]
		put_str	endl
		jmp	exit

else:		put_str	try
		put_str	endl

exit:		movsd	[ans], xmm7
		put_dbl	[ans]
		put_str	endl		

alldone: 	mov	ebx,0		; return 0
		mov	eax,1		; on
		int	80h		; exit

