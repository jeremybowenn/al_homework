;
; Builds nerd cards and uses bit operations to perform various actions.
;
; To compile:
; gcc -c readcodes.c
; nasm -f elf64 nerdcard.asm
; gcc nerdcard.o readcodes.o
; ./a.out
;
; A code record looks like this:
;
; Code {
;	int id;
;	int score;
;	char description[256];
; };
;
;
; A nerd record will look like this:
;
; Nerd {
;	char name[80];
;	long code;
; };
;
%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"
%define ID 0
%define SCORE 4
%define DESCRIPTION 8
%define NAME 0
%define CODES 80
%define	RECORD 88

		extern readCodes
		section .data
codefile:	db	"codes.txt",0
nerdfile:	db	"nerds.txt", 0
allstring:	db	"Achievements Everyone Has ....", 0
nonestring:	db	"Achievements Nobody Has ....", 0
line:		db	"-------------------------------------------", 0
space:		db	" ",0
endl		db	10,0


		section .bss
codes:		resq	64		; up to 64 codes are supported
coden:		resq	1		; number of nerds we have
nerds:		resb	88*50		; 144 bytes for each nerd, 50 nerds
fp:		resq	1
temp:		resd	1

		section .text
		global 		main
main:

		; coden= readCodes(codes,codefile)
		mov	rdi,codes
		mov	rsi,codefile
		call	readCodes
		mov	[coden],rax

;		put_i	[coden]
;		put_str	endl

		call	readNerds
		sub	rax, 1
		mov	r13, rax
		mov	rdi, rax
		call	displayNerds

		mov	rdi, r13
		call	doneByAll
		
		mov	rdi, r13
		call	doneByNone
		; displayCodes(codes,n)
;		mov	rdi,codes
;		mov	rsi,[coden]
;		call	displayCodes

theend:
		mov     eax, 60
		xor     rdi, rdi
		syscall

;--------------------------------------------------------------------------------
; readNerds()
;---------------------------
; reads in nerds
;
readNerds:
		xor	rcx, rcx
		xor	rdx, rdx
		mov	r8, nerds
		mov	r9, temp
		fopenr	[fp], nerdfile
		xor	rbx, rbx

readNerdsLoop:	
		cmp	ebx, -1
		je	nerdsStop
		fget_str [fp], r8
		mov	rbx, rax
		xor	rax, rax
getCodesLoop:	
		cmp	eax, -1
		je	increment	
		fget_i	[fp], [r9]
		mov	r10, 1
		mov	cl, [r9]
		shl	r10, cl
		or	[r8+CODES], r10

		jmp	getCodesLoop
increment:
		inc	rdx
		add	r8, RECORD
		xor	rax, rax
		jmp	readNerdsLoop

nerdsStop:	
		mov	rax, rdx
		ret	


;--------------------------------------------------------------------------------
; displayNerds(n)
;---------------------------
; displays nerds
;
displayNerds:
		mov	r8, nerds
		xor	rdx, rdx

displayLoop:	
		cmp	rdx, rdi
		jge	stopDisplay
		put_i	edx
		put_str	space		
		put_str	r8
		put_str	space
		mov	r9, [r8+CODES]
		put_i	r9d
		put_str endl	

displayInc:
		inc	rdx
		add	r8, RECORD
		jmp	displayLoop

stopDisplay:
		ret

;--------------------------------------------------------------------------------
; doneByAll(n)
;---------------------------
; Displays the achievements done by all nerds
;

doneByAll:
		xor	rdx, rdx	
		mov	r8, nerds
		mov	r9,0xFFFFFFFFFFFFFFFF 

		put_str	endl
		put_str	line
		put_str	endl
		put_str	allstring
		put_str	endl
		put_str	line
		put_str	endl
doneByAllLoop:	
		cmp	rdx, rdi
		jge	displayDone

		mov	r10, [r8+CODES]
		and	r9, r10

		inc	rdx
		add	r8, RECORD
		jmp	doneByAllLoop

displayDone:	
		xor	rcx, rcx
displayDoneLoop:
		cmp	rcx, 64
		jge	stopDoneByAll

		mov	r15, 1
		shl	r15, cl
		and	r15, r9
		cmp	r15, 0
		je	displayDoneInc
		mov	r14, [codes+8*rcx]
		put_i	[r14]
		put_str	space
		add	r14, DESCRIPTION
		put_str	r14	
		put_str	endl
;		put_str	r14
;		put_str	space

displayDoneInc:
		inc	rcx
		jmp	displayDoneLoop


stopDoneByAll:
		ret



;--------------------------------------------------------------------------------
; doneByNone(n)
;---------------------------
; Displays the achievements done by no nerds
;

doneByNone:
		xor	rdx, rdx	
		mov	r8, nerds
		mov	r9, 0

		put_str	endl
		put_str	line
		put_str	endl
		put_str	nonestring
		put_str	endl
		put_str	line
		put_str	endl
doneByNoneLoop:	
		cmp	rdx, rdi
		jge	displayNoneDone

		mov	r10, [r8+CODES]
		or	r9, r10

		inc	rdx
		add	r8, RECORD
		jmp	doneByNoneLoop

displayNoneDone:	
		xor	rcx, rcx
displayNoneLoop:
		cmp	rcx, [coden]
		jge	stopDoneByNone

		mov	r15, 1
		shl	r15, cl
		and	r15, r9
		cmp	r15, 0
		jne	displayNoneInc
		mov	r14, [codes+8*rcx]
		put_i	[r14]
		put_str	space
		add	r14, DESCRIPTION
		put_str	r14	
		put_str	endl
;		put_str	r14
;		put_str	space

displayNoneInc:
		inc	rcx
		jmp	displayNoneLoop


stopDoneByNone:
		ret

;--------------------------------------------------------------------------------
; displayCodes(codes,n)
;---------------------------
; display codes along with id numbers
;
displayCodes:
		xor	rcx,rcx

displayCodesLoop:
		cmp	rcx,rsi
		jge	endDisplayCodes
		mov	r8,[rdi+8*rcx]
		put_i	[r8+ID]
		put_ch	[space]
		put_i	[r8+SCORE]
		put_ch	[space]
		add	r8,DESCRIPTION
		put_str	r8
		put_str	endl
		inc	rcx
		jmp	displayCodesLoop

endDisplayCodes:
		mov	rax,rcx
		ret
