;; LongLiveTheDog.asm
;;
;; Converts a dog's age from human years to dog years.
;;
;; by Jeremy Bowen
;; 9/8/18
;;

%include "../lib/iomacros.asm"

		global main
		section .data

name_prompt:	db	"What is your dog's name? ",0
age_prompt:	db	"What is your dog's age in human years? ", 0
readstr:        db	"%s", 0
readint:	db	"%d", 0
dogyears_int:	dd	7
answer1_str:	db	" is ", 0
answer2_str:	db	" dog years old.", 0
endl:		db	10,0

		section .bss

age:		resd	1
name:		resb	80

		section .text

main:
		put_str	name_prompt

		get_str name	
		
		put_str	age_prompt

		get_i	[age]
		
		put_str	name
		put_str	answer1_str

		mov	ebx, [dogyears_int]
		mov	eax, [age]
		mul 	ebx

		put_i	eax	
		put_str answer2_str
		put_str	endl

		mov     eax, 60                 ; system call 60 is exit
		xor     rdi, rdi                ; exit code 0
		syscall








