;; A program to return the tallest and heaviest sumo wrestlers using dynamic memory allocation  
;; 
;; Jeremy Bowen
;;
;; 10/21/18
;;

%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"
%define RANK 20
%define HEIGHT 24
%define WEIGHT 28
%define RECSIZE 32

		global main

		section .data
tallest:	db	"Tallest:", 0
heaviest:	db	"Heaviest:", 0
line:		db	"----------", 0
file_name:	db	"../data/sumo.txt", 0
endl:		db	10,0

		section .bss
wrestlers:	resq	100	
fp:		resq	1

		section .text
main:		
		fopenr	[fp], file_name
		mov	r15, wrestlers
		xor	r14, r14

loop:		cmp	eax, -1
		je	cmp	
		xor	rdi, rdi
		xor	rsi, rsi
		xor	rdx, rdx
		xor	r10, r10
		xor	r8, r8
		xor	r9, r9
		
		mov	rdi, 0
		mov	rsi, 32
		mov	rdx, 0x3
		mov	r10, 0x22
		mov	r8, -1
		mov	r9, 0
		mov	rax, 9

		syscall

		mov	[r15], rax
		mov	r9, [r15]
		fget_i	[fp], [r9+RANK]
		fget_i	[fp], [r9+HEIGHT]
		fget_i	[fp], [r9+WEIGHT]
		fget_ch	[fp], r10b
		fget_str [fp], r9

		add	r15,8 
		inc	r14
		jmp	loop

cmp:		mov	r15, wrestlers
		sub	r14, 1 
		xor	r13, r13	;counter
		mov	r8, [r15]		;tallest
		mov	r9, [r15]		;heaviest
		
cmploop:	cmp	r14, r13
		je	exit
		
		mov	r10, [r15]
		mov	r11d, [r10+HEIGHT]
		mov	r12d, [r10+WEIGHT]

height:		cmp	r11d, [r8+HEIGHT]
		jle	weight
		mov	r8, r10

weight:		cmp	r12d, [r9+WEIGHT]
		jle	inc
		mov	r9, r10

inc:		inc	r13
		add	r15,8 
		jmp	cmploop



exit:
		put_str	tallest
		put_str	endl
		put_str	line
		put_str	endl
		put_str	r8
		put_str	endl
		put_i	[r8+RANK]
		put_str	endl
		put_i	[r8+HEIGHT]
		put_str	endl
		put_i	[r8+WEIGHT]
		put_str	endl
		put_str	endl

		put_str	heaviest
		put_str	endl
		put_str	line
		put_str	endl
		put_str	r9
		put_str	endl
		put_i	[r9+RANK]
		put_str	endl
		put_i	[r9+HEIGHT]
		put_str	endl
		put_i	[r9+WEIGHT]
		put_str	endl
		
		fclosem	[fp]
		mov	eax, 60
		xor	rdi, rdi
		syscall
	
