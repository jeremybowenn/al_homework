;; A program for the first lab day
;; Jeremy Bowen
;; 9/13/18

%include "../lib/dumpregs.asm"

		global main
		section .data

int:		dd	68, -9
floaty:		dd	-68.125
char:		db	"Greetings!", 10, 0
bighex:		dq	10h
mediumbin:	dw	10b
endl:		db	10, 0

		section .bss

somememory:	resd	2

		section .data
nightnight:	db	"ZZZZZZZZ"


		section .text
main:

;		mov 	r8, int
;		mov 	r9, floaty
;		mov	r10, [int]
;		mov	r11, [floaty]
;		dump_regs		
	
		mov 	qword [somememory], 0x33333333
	
		mov	rax, [int]
		mov	rbx, [int+8]
		mov	rcx, [int+16]
		mov	rdx, [int+24]
		mov	r8, [int+32]
		mov	r9, [int+40]
		mov	r10, [int+48]
		mov	r11, [int+56]
		mov	r12, [int+64]
		mov	r13, [int+72]
		mov	r14, [int+80]

		dump_regs

		mov	eax, 60
		xor	rdi, rdi
		syscall
