;; A program to to help determine how much change one needs
;;
;; Jeremy Bowen
;; 9/15/18
;;

%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"		
		global main 
		section .data
change_prompt:	db	"Enter change (0-99): ", 0
service_str:	db	"Service fee: ", 0
remaining_str:	db	"Remaining change: ", 0
quarters_str:	db	"Quarters: ", 0
dimes_str:	db	"Dimes: ", 0
nickels_str:	db	"Nickels: ", 0
pennies_str:	db	"Pennies: ",  0
endl:		db	10

		section .bss
change:		resd	1
remaining:	resd	1
quarters:	resd	1
dimes:		resd	1
nickels:	resd	1
pennies:	resd	1

		section .text
main:
		put_str	change_prompt
		get_i	[change]
		
		mov	eax, 5
		imul	eax, [change]

		xor	edx, edx
		mov	ebx, 100
		idiv	ebx

		put_str	service_str
		put_i	eax
		put_str	endl

		mov	ecx, [change]
		sub	ecx, eax
		mov	[remaining], ecx	

		put_str	remaining_str
		put_i	[remaining]
		put_str	endl
	
		xor	edx, edx
		mov	eax, [remaining]
		mov	ebx, 25
		idiv	ebx

		put_str	quarters_str
		put_i	eax
		put_str	endl

		mov	eax, edx	
		mov	ebx, 10
		xor	edx, edx
		idiv	ebx
	
		put_str	dimes_str
		put_i	eax
		put_str	endl

		mov	eax, edx
		mov	ebx, 5
		xor	edx, edx
		idiv	ebx

		put_str	nickels_str
		put_i	eax
		put_str	endl
	
		put_str	pennies_str
		put_i	edx
		put_str	endl

		mov	eax, 60
		xor	rdi, rdi
		syscall
