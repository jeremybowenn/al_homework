;
; A macro for lexigraphically comparing strings. 
;
; Jeremy Bowen
; 10/28/18
;

%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

%macro	cmp_str	2
		push	rbx
		push	rcx
		push	rdx

		mov	rbx, %1	
		mov	rax, %2
		xor	rdx, rdx

		
%%loop:		mov	cl, [rax+rdx]
		cmp	[rbx+rdx],cl 	;checks each char in each string
		jl 	%%less	
		jg	%%great
		
		cmp	cl, 0
		je	%%equal		;checks if it is the end of each string since the chars are equal
	
		inc	rdx
		jmp	%%loop

%%great:	mov	rax, 1
		cmp	rax, -1		;sets flag(s) and returns
		jmp	%%done

%%less:		mov	rax, -1		
		cmp	rax, 1
		jmp	%%done

%%equal:	mov	rax, 1
		cmp	rax, 1
		jmp	%%done		

%%done:		
		pop	rdx
		pop	rcx
		pop	rbx
	
%endmacro
		global main

		section	.data
a:		db	"", 0
b:		db	"ABE", 0
endl:		db	10,0

		section .text

main:		mov	r8, a
		mov	r9, b
		cmp_str	r8, r9
		je	equal
		jl	less
		jg	great

equal:		xor	rax, rax	;if(a==b){print(0)}
		put_i	eax
		put_str	endl
		jmp	exit

less:		mov	rax, -1		;if(a<b){print(-1)}
		put_i	eax
		put_str	endl	
		jmp	exit

great:		mov	rax, 1		;if(a>b){print(1)}
		put_i	eax
		put_str	endl
		jmp	exit

exit:		mov     eax, 60                 ; system call 60 is exit
		xor     rdi, rdi                ; exit code 0
		syscall
