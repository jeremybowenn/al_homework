;
; labday.asm
;
; Playing around with the stack.
;

%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

%macro	madd	2
	push	rbx
	push	rcx

	mov	ebx, %1
	mov	ecx, %2
	add	ebx, ecx
	mov	eax, ebx
	
	pop	rcx
	pop	rbx
	
%endmacro
		global main

		section	.data
a:		dd	5
b:		dd	4
endl:		db	10,0

		section .text

main:		madd	[a], [b]
		dump_regs
		put_str endl
		mov	r8d, 5
		mov	r9d, 5
		madd	r8d, r9d
		dump_regs

		put_str	endl
		mov	ebx, 5
		mov	ecx, 6 
		madd	ebx, ecx
		dump_regs

		mov     eax, 60                 ; system call 60 is exit
		xor     rdi, rdi                ; exit code 0
		syscall
