;; A program for reading in strings from names.txt and 
;; returning the strings with lengths greater than 20  
;;  
;; 
;; Jeremy Bowen
;;
;; 9/22/18
;;

%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"
%define RANK 20
%define HEIGHT 24
%define WEIGHT 28
%define RECSIZE 32

		global main

		section .data
file_name:	db	"../data/sumo.txt", 0
tallest:	db	"Tallest:", 0
heaviest:	db	"Heaviest:",0 
line:		db	"-------------", 0
endl:		db	10,0

		section .bss
wrestlers:	resb	100*RECSIZE	
fp:		resq	1

		section .text
main:		
		fopenr	[fp], file_name
		mov	r11, wrestlers
		xor	rcx, rcx
	
loop:		cmp	eax, -1
		je	cmp	
		fget_i	[fp],[r11+RANK] 
		fget_i	[fp], [r11+HEIGHT]
		fget_i	[fp], [r11+WEIGHT]
		fget_ch	[fp], r15b
		fget_str [fp], r11
		inc	rcx
		add	r11, RECSIZE
		jmp	loop

cmp:		xor	rdx, rdx
		sub	rcx, 1
		mov	r11, wrestlers
		mov	r8, r11	;tallest
		mov	r9, r11	;heaviest
	
cmp_loop:	cmp	rdx, rcx
		je	print
		mov	r10d, [r11+HEIGHT]
		mov	r12d, [r11+WEIGHT]

height:		cmp	r10d, [r8+HEIGHT]
		jle	weight
		mov	r8, r11

weight:		cmp	r12d, [r9+WEIGHT]
		jle	inc
		mov	r9, r11

inc:		inc	rdx
		add	r11, RECSIZE
		jmp	cmp_loop

print:		put_str	tallest
		put_str	endl
		put_str	line
		put_str	endl
		put_str	r8
		put_str	endl
		put_i	[r8+RANK]
		put_str	endl
		put_i	[r8+HEIGHT]
		put_str	endl
		put_i	[r8+WEIGHT]
		put_str	endl
		put_str	endl

		put_str	heaviest
		put_str	endl
		put_str	line
		put_str	endl
		put_str	r9
		put_str	endl
		put_i	[r9+RANK]
		put_str	endl
		put_i	[r9+HEIGHT]
		put_str	endl
		put_i	[r9+WEIGHT]
		put_str	endl
		put_str	endl
		fclosem	[fp]

exit:		mov	eax, 60
		xor	rdi, rdi
		syscall
		
