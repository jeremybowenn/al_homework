;; A program for reading in strings from names.txt and 
;; returning information about the file and its contents   
;;  
;; 
;; Jeremy Bowen
;;
;; 11/11/18
;;

%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

		global main

		section .data
file_name:	db	"../data/names.txt", 0
endl:		db	10,0
lengthstr:	db	"Length: ", 0
filestr:	db	"File name: ", 0
indexstr:	db	"Index of longest string: ", 0
namestr:	db	"Longest name: ", 0
line:		db	"----------------------------", 0

		section .bss
strings:	resb	10000*27	; a string array with 10000 slots where no string is more than 26 chars with a null char
length:		resd	1		; an int to hold the length of the array

		section .text
		extern	readStrings
main:		
		xor	rdi, rdi
		xor	rsi, rsi
		mov	rdi, strings
		mov	rsi, file_name

		call	readStrings	;reads in all the names
		mov	[length], eax
		put_str	endl
		put_str	endl
		put_str	line
		put_str	endl
		put_str	endl
		
		put_str	filestr		;print(file_name)
		put_str	file_name
		put_str	endl

		put_str	lengthstr	;print(length)
		put_i	[length]
		put_str	endl

		put_str	indexstr
		mov	rdi, strings
		mov	rsi, [length]
		call 	findLongPos	;calling findLongPos to get index
		mov	r9, rax
		sub	rax, strings
		mov	r8, 27
		idiv	r8		;getting the index in the proper form
		put_i	eax		;print(index)
		
		put_str	endl

		put_str	namestr
		put_str	r9		;print(name)
		put_str	endl


exit:		mov	eax, 60
		xor	rdi, rdi
		syscall


		; A function to find the length of a given str
		;
		; @param A string's address
		; @returns The string's length

strlen:		
		xor	rax, rax
loop:		mov	bl, [rdi+rax]	;comparing each byte with 0 to find end
		cmp	bl, 0
		je	done
		inc	rax
		jmp	loop

done:		ret




		; A function to find the position of the longest string in an array
		;
		; @param address of an array
		; @param the array's length
		; @returns the index of the longest string in the array

findLongPos:	
		xor	rcx, rcx	;counter
		xor	r8, r8		;max
		xor	r9, r9		;index		

loop2:		cmp	rcx, rsi	;check to see is we have reached the end of the array
		jge	finish
		call	strlen		;get the current sttring's length
		cmp	rax, r8
		jle	inc
		mov	r8, rax		;change the max and address for the new string
		mov	r9, rdi

inc:		add	rdi, 27		;increment by 27
		inc	rcx
		jmp	loop2


finish:		mov	rax, r9		;return in rax
		ret





