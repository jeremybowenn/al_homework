;; A program that chooses a random numbr and asks the user to guess it
;; 
;; Jeremy Bowen
;;
;; 9/20/18
;;

%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

		global main

		section .data
guess_prompt:	db	"Guess a number.", 10, 0
higher_prompt:	db	"Higher.", 10, 0
lower_prompt:	db	"Lower.", 10, 0
congrats_str:	db	"Congratulations! You got it!", 10, 0
endl		db	10

		section .bss

		section .text
main:

random:		add	eax, ebx
		add	eax, ecx
		add	eax, edx
		add	eax, esi
		add	eax, edi
		add	eax, r8d
		add	eax, r9d
		add	eax, r10d
		add	eax, r11d
		add	eax, r12d
		add	eax, r13d
		add	eax, r14d
		add	eax, r15d
		xor	edx, edx
		mov	ebx, 100
		idiv	ebx
		add	edx, 1
		
		put_i	edx	
		put_str	endl	
		put_str	guess_prompt

loopentry:	get_i	ebx
		cmp	edx, ebx	
		je	congrats
		jl	lessthan
		put_str	higher_prompt
		jmp	loopentry

lessthan:	put_str	lower_prompt
		jmp	loopentry

congrats:	put_str congrats_str

		mov 	rax, 60		;exit	
		xor	rdi, rdi
		syscall
