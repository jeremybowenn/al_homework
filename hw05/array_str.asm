;; A program for reading in strings from names.txt and 
;; returning the strings with lengths greater than 20  
;;  
;; 
;; Jeremy Bowen
;;
;; 9/22/18
;;

%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

		global main

		section .data
file_name:	db	"../data/names.txt", 0
endl:		db	10,0

		section .bss
strings:	resb	10000*26	; a string array with 10000 slots where no string is more than 26 chars
length:		resb	10000*4		; a parallel int array for lengths
fp:		resq	1

		section .text
main:		
		fopenr	[fp], file_name
		xor	rcx, rcx		; counter
		mov	r8, strings

loop:		fget_str [fp], r8
		add	r8, 26
		mov	[length+4*rcx], eax
		inc	rcx
		cmp	eax, -1
		jne	loop

display:	xor	rdx, rdx
		mov	r8, strings

print_loop:	cmp	rdx, rcx
		je	done
		
		cmp	dword [length+4*rdx], 20
		jl	inc	
		put_str	r8
		put_str	endl
inc:		add	r8, 26
		inc	rdx
		jmp	print_loop
done:
		fclosem	[fp]


exit:		mov	eax, 60
		xor	rdi, rdi
		syscall
		
