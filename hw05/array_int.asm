;; A program for reading in the numbers.txt file and printing
;;  the max, min and average of the numbers
;;  
;; 
;; Jeremy Bowen
;;
;; 9/22/18
;;

%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

		global main

		section .data
line:		db	"----------------", 0
total:		db	"Total: ", 0
min:		db	"Min: ", 0
max:		db	"Max: ", 0
avg:		db	"Avg: ", 0
file_name:	db	"../data/numbers.txt", 0
endl:		db	10,0

		section .bss
a:		resd	10000*4   ; an int array with 10000 slots
fp:		resq	1

		section .text
main:		
		fopenr	[fp], file_name
		

		xor	r8, r8
read_loop:	fget_i	[fp], r9d
		mov	[a+4*r8], r9d
		inc	r8
		cmp	rax, 1
		je	read_loop

done:
		fclosem	[fp]

		xor	r9, r9; counter
		xor	r10, r10; max
		xor	r11, r11; min
		xor	r12, r12; average

calc_loop:	cmp 	r9, r8
		je	print	
		mov	r15d, [a+4*r9]
;max		
		cmp	r15d, r10d
		jle	x
		mov	r10d, r15d

;min		
x:		cmp	r15d, r11d
		jge	y
		mov	r11d, r15d

;avg		
y:		add	r12d, r15d
		
		inc	r9
		jmp	calc_loop
			
print:		mov	eax, r12d
		cdq
		idiv	r8d
		mov	r12d, eax
	
		put_str	file_name
		put_str	endl
		put_str	line
		put_str	endl
		put_str	total
		put_i	r8d
		put_str	endl
		put_str	max
		put_i	r10d
		put_str	endl
		put_str	min
		put_i	r11d
		put_str	endl
		put_str	avg	
		put_i	r12d
		put_str	endl

exit:		mov	eax, 60
		xor	rdi, rdi
		syscall
		
