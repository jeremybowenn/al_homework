;; A program that outputs number between 2 and 100,000 whose
;; hailstone sequence if more then 300 
;; 
;; Jeremy Bowen
;;
;; 9/22/18
;;

%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

		global main

		section .data
length_str	db	" has length ", 0
endl		db	10

		section .bss

		section .text
main:
		mov	r8d, 1
		mov	r10d, 100000
		mov	r11d, 300
 
start:		inc	r8d
		cmp	r8d, 100000
		je	done
		mov	r9d, r8d
		mov	r15d,0 

conditional:	cmp 	r9d, 1
		je	print
		inc	r15d
		mov	eax, r9d
		mov	ebx, 2
		xor	edx, edx
		idiv	ebx
		cmp	edx, 0
		je	even
		jg	odd

odd:		mov	ebx, 3
		imul	r9d, ebx
		inc	r9d
		jmp	conditional

even:		mov	r9d, eax
		jmp	conditional

print:		cmp	r15d, r11d
		jl	start

		put_i	r8d
		put_str	length_str
		put_i	r15d
		put_str	endl
		jmp	start
done:
		

		mov 	rax, 60		;exit	
		xor	rdi, rdi
		syscall
